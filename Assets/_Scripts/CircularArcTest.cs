﻿using UnityEngine;

public class CircularArcTest : MonoBehaviour
{
	[SerializeField] private float maxAngle = 30f;
	[SerializeField] private float radius = 1f;
	[SerializeField] private float height = 5f;
	[SerializeField] private int numOfPoints = 4;
	private Vector3 centerPoint;
	private Vector3 firstPoint;
	private Vector3 secondPoint;

	void Start()
	{
	}

	Vector3 GetPointOnCircle(float _angle)
	{
		float a = _angle * Mathf.Deg2Rad;
		firstPoint = centerPoint + new Vector3(radius, 0f, 0f);
		float x = Mathf.Cos(a) * radius;
		float z = Mathf.Sin(a) * radius;
		return new Vector3(x, 0f, z);
	}

	void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(centerPoint, 0.3f);

		centerPoint = transform.position;
		float stepAngle = maxAngle / numOfPoints;
		for (int i = 0; i < numOfPoints; i++)
		{
			Vector3 p = centerPoint + GetPointOnCircle(stepAngle * i);
			p = new Vector3(p.x, (i * height) / numOfPoints, p.z);
			Gizmos.DrawWireSphere(p, 0.3f);
		}
	}
}