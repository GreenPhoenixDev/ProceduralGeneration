﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Track))]
public class TrackInspector : Editor
{
	private enum EditOption { None, Waypoints, Profile, ConnectMesh }
	private EditOption editOption = EditOption.None;
	private enum HandleType { FreeMove, Arrows, Rotation }
	private HandleType handleType = HandleType.FreeMove;

	public override void OnInspectorGUI()
	{
		Track track = target as Track;
		editOption = (EditOption)EditorGUILayout.EnumPopup("Edit Option", editOption);

		switch (editOption)
		{
			case EditOption.None:
				break;
			case EditOption.Waypoints:
				handleType = (HandleType)EditorGUILayout.EnumPopup("Handle Type", handleType);

				if (GUILayout.Button("Add Waypoint"))
				{
					track.AddWaypoint();
					track.CreateMesh();
				}
				else if (GUILayout.Button("Remove Last Waypoint"))
				{
					track.RemoveLastWaypoint();
					track.CreateMesh();
				}
				else if (GUILayout.Button("Update Mesh"))
				{
					track.CreateMesh();
				}

				SerializedProperty newWpDist = serializedObject.FindProperty("NewWpDist");
				EditorGUILayout.PropertyField(newWpDist);
				SerializedProperty waypoints = serializedObject.FindProperty("Waypoints");
				EditorGUILayout.PropertyField(waypoints, true);

				break;
			case EditOption.Profile:
				if (GUILayout.Button("Add Profile Point"))
				{
					track.AddProfilePoint();
					track.CreateMesh();
				}
				else if (GUILayout.Button("Remove Last Profile Point"))
				{
					track.RemoveLastProfilePoint();
					track.CreateMesh();
				}
				else if (GUILayout.Button("Update Mesh"))
				{
					track.CreateMesh();
				}

				SerializedProperty profile = serializedObject.FindProperty("Profile");
				EditorGUILayout.PropertyField(profile, true);
				break;
			case EditOption.ConnectMesh:
				if (GUILayout.Button("Connect Mesh Ends"))
				{
					track.ConnectMeshEnds();
					track.CreateMesh();
				}
				else if(GUILayout.Button("Cut Mesh Ends"))
				{
					track.CutMeshEnds();
					track.CreateMesh();
				}
				break;
			default:
				break;
		}
		serializedObject.ApplyModifiedProperties();
	}

	void OnSceneGUI()
	{
		Track track = target as Track;

		switch (editOption)
		{
			case EditOption.None:
				break;
			case EditOption.Waypoints:
				EditWaypoints(track);
				break;
			case EditOption.Profile:
				EditProfile(track);
				break;
			default:
				break;
		}

	}

	void EditWaypoints(Track track)
	{
		if (track.Waypoints == null) { return; }
		for (int i = 0; i < track.Waypoints.Count; i++)
		{
			bool isFirst = i == 0;              // if i == 0 then isFirst is true, else false
			if (!isFirst)
			{
				Vector3 pos1 = track.transform.TransformPoint(track.Waypoints[i].Position);
				Vector3 pos2 = track.transform.TransformPoint(track.Waypoints[i - 1].Position);
				Handles.color = Color.magenta;
				Handles.DrawDottedLine(pos1, pos2, 4f);
			}

			Handles.color = isFirst ? Color.green : Color.magenta;
			Vector3 oldPos = track.transform.TransformPoint(track.Waypoints[i].Position);
			Vector3 newPos = oldPos;
			Quaternion oldRot = track.Waypoints[i].Rotation;
			Handles.Label(oldPos + Vector3.up, i.ToString());
			Quaternion newRot = oldRot;
			switch (handleType)
			{
				case HandleType.FreeMove:
					newPos = Handles.FreeMoveHandle(oldPos, Quaternion.identity, 0.5f, Vector3.one, Handles.DotHandleCap);
					break;
				case HandleType.Arrows:
					newPos = Handles.PositionHandle(oldPos, oldRot);
					break;
				case HandleType.Rotation:
					newRot = Handles.RotationHandle(oldRot, oldPos);
					break;
				default:
					break;
			}

			if (newPos != oldPos)
			{
				track.Waypoints[i].Position = track.transform.InverseTransformPoint(newPos);
				track.CreateMesh();
			}

			if (newRot != oldRot)
			{
				track.Waypoints[i].Rotation = newRot;
				track.CreateMesh();
			}
		}
	}

	void EditProfile(Track track)
	{
		List<Vector3> points = track.Profile.Points;

		if (points == null) { return; }

		for (int i = 0; i < points.Count; i++)
		{
			bool isFirst = i == 0;
			if (!isFirst)
			{
				Handles.color = Color.yellow;
				Vector3 pos1 = track.transform.TransformPoint(points[i]);
				Vector3 pos2 = track.transform.TransformPoint(points[i - 1]);
				Handles.DrawLine(pos1, pos2);
			}

			Handles.color = isFirst ? Color.green : Color.blue;
			Vector3 oldPoint = track.transform.TransformPoint(points[i]);
			Vector3 newPoint = Handles.FreeMoveHandle(oldPoint, Quaternion.identity, 0.2f, Vector3.one * 0.25f, Handles.DotHandleCap);
			newPoint = new Vector3(newPoint.x, newPoint.y, 0f);

			if (newPoint != oldPoint)
			{
				points[i] = track.transform.InverseTransformPoint(newPoint);
				track.CreateMesh();
			}
		}
	}
}
